<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= $data["judul"]; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container text-center mt-4">
        <h1>Halaman User</h1> 
        <img  src="<?=BASEURL ?>/img/fotoku1.jpg" alt="" class="rounded-circle shadow " style="width:20%; height:10;">
        <p>Halo, nama saya <?= $data["nama"]; ?>, saya seorang <?= $data["pekerjaan"];?></p>
        </div>
    </body>
</html>