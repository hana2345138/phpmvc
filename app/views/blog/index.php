<!-- Button trigger modal -->

<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
    Launch demo modal
</button>

<div class="container mt-5">
    <div class="row">
        <div class="col-6">
            <h3>Blog</h3>
            <ul class="list-group">
                <?php foreach ($data["blog"] as $blog) : ?>
                    <li class="list-group-item list-group-item d-flex justify-content-between align-items-center">
                        <?= $blog["judul"]; ?>
                        <?= $blog["penulis"]; ?>
                        <a href="<?= BASEURL; ?>/blog/detail/<?= $blog["id"]; ?>" class="badge badge-primary">baca</a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Buat Artikel</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>/blog/tambah" method="POST">
                    <div class=”form-group”>
                        <label for=”judul”>Judul Artikel</label>
                        <input type=”text” class=”form-control” id=”judul” name="judul">
                    </div>
                    <div class="form-group">
                        <label for="tulisan">Isi Artikel</label>
                        <textarea class="form-control" id="tulisan" rows="5" name="tulisan"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="penulis">Nama Penulis</label>
                        <input type="text" class="form-control" id="penulis" name="penulis">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah Artikel</button>
                </form>
            </div>
        </div>


    </div>
</div>
</div>