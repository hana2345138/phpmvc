<?php 
class App{

    protected $controller = "Home";
    protected $method = "index";
    protected $params = [];

    

    public function __construct(){
        $uri = trim($_SERVER['REQUEST_URI'], '/');
        $script=trim($_SERVER['SCRIPT_NAME'], '/');
        $uri = str_replace('/index.php', '', $uri);
        $script = str_replace('index.php', '', $script);
        $uri= str_replace($script,"", $uri);
            $url = $this->parseURL($uri);
            
            
        //     var_dump($url);
        
        if (file_exists('../app/controllers/'.$url[0].'.php')) {
            $this->controller = $url[0];
            unset($url[0]);
        }

        require_once '../app/controllers/'.$this->controller.'.php';
        $this->controller = new $this->controller;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1]; unset($url[1]);
            }
        }
        if(!empty($url)){
            $this->params = array_values ($url);
        }
        call_user_func_array([$this->controller, $this->method], $this->params);
           
        }
    

    public function parseURL($uri)
        {
         
                // $url = $_GET['url'];return $url;
                $url = trim($uri, '/');
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/',$url);
                return $url;
            
        } 
}





?>