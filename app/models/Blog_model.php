<?php 
class Blog_model{
    // private $blog = [
    //     ["penulis" =>"Linux", "judul"=>"Belajar PHP MVC ","tulisan"=>""],
    //     ["penulis"=>"Linux","judul"=>"Belajar OOP PHP","tulisan"=>"Tutorial PHP OOP"],
    //     ["penulis"=>"Linux","judul"=>"Belajar PHP Dasar","tulisan"=>"Tutorial PHP Dasar"]
    // ];
//     private $dbh;
//     private $stmt;
//     public function __construct(){
//         //  data source name
//          $dsn="mysql:host=127.0.0.1;dbname=phpmvc";
//         try{
//             $this->dbh = new PDO($dsn, "root", "");

//         }catch (PDOException $e){
//             die($e->getMessage());
//         }
//     }
    
        
    
    
//     public function getAllBlog(){
//         $this->stmt = $this->dbh->prepare("SELECT * FROM blog");
//         $this->stmt->execute();
//         return $this->stmt->fetchAll(PDO::FETCH_ASSOC);

//     }
// }
private $table = "blog";
private $db;

public function __construct(){
    $this->db = new Database;

}
public function getAllBlog(){
    $this->db->query("SELECT * FROM ". $this->table);
    return $this->db->resultAll();

}
public function getBlogById($id){
    $this->db->query("SELECT * FROM " . $this->table. ' WHERE id=:id');
    $this->db->bind('id',$id);
    return $this->db->resultSingle();
}

public function buatArtikel($data){
    $query = "INSERT INTO blog (penulis, judul, tulisan) VALUES (:penulis, :judul , :tulisan)";
    $this->db->query($query);
    $this->db->bind('judul',$data['judul']);
    $this->db->bind('tulisan',$data['tulisan']);
    $this->db->bind('penulis',$data['penulis']);
    $this->db->execute();
    return $this->db->rowCount();
}
}

?>